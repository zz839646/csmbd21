#0: Basic MapReduce
#1: Using functions and sorting of the output
#2: checking input, and preparing to implement multithreading
#3 (final): everything refactored into functions, multithreading implemented
#4 (final v2): input reading moved into pool, reducer now returns a list not a dictionary to make sorting simpler

#This solution is adapted from the tutorial examples, in particular the multithreaded mapreduce example

#First we import various packages
#re provide regex
import re
#multiprocessing provides support for threading
import multiprocessing as mp

#Since the number of CPUs is a constant we will be using several times we can assign it to a variable
CORES = mp.cpu_count()

#We can then define some functions
#Because we're using multiprocessing pools any code we want to run in parallel needs to be in a function
#Both the mapper and reducer will run in parallel, but the combiner won't, I explain why when the combiner is actually run
#First the mapper which cleans the input line, extracts and tests the passenger id then returns it and 1
def mapper(inputLine):
    #First the input line is cleaned of any leading or trailing whitespace
    inputLine = inputLine.strip()
    #Then the passenger ID is extracted, it should be the first 10 characters of the line
    passengerID = inputLine[: 10]
    #We check for errors in the passenger ID by doing a regex search
    #The regex search is looking 3 capital letters followed by 4 numbers followed by 2 capital letters and a number
    #Which is the given format for passenger IDs
    #We check if the line DOES NOT match this condition
    if not re.search("([A-Z]){3}[0-9]{4}[A-Z]{2}[0-9]", passengerID):
        #If it does not match the condition we print the ID and a message
        print(passengerID, "Is not a valid passenger ID")
    else:
        #If the line does match the format for passenger ID we return it and the count (1)
        return(passengerID, 1)
   
#Next is the combiner which takes the output of the mappers and organises it by key
def combiner(mapData):
    #First create a dictionary
    combinedData = {}
    #Then it goes through the output of the mapper
    for key, value in mapData:
        #If the key is not in the dictionary
        if key not in combinedData.keys():
            #Add the value to a list with the key as the key
            combinedData[key] = [value]
        else:
            #Or append the new count to the that key's list
            combinedData[key].append(value)
    #Finally return the dictionary
    return combinedData

#Finally the reducer which takes the combined data and sums up the counts for the key
def reducer(combinedOut):
    #First we create a list
    reducedData = []
    #Then we extract the key and values from the input
    key = combinedOut[0]
    values = combinedOut[1]
    #And initalise a counter variable at 0
    count = 0
    #We then loop through the values
    for x in values:
        #And add them to the counter variable
        count += x
    #We add the key/count the list
    reducedData.append(key)
    reducedData.append(count)
    #And return it
    return(reducedData)

#With the functions defined we can set up the multithreading pools
#I don't know enough about multithreading to say why, but it complains if the pool code isn't in an if like this
if __name__ == '__main__':
    #We create a pool object to manage multithreading, with the number of cores being the constant calculated at the top of the file
    with mp.Pool(processes = CORES) as pool:
        #The first thing we need to do is import the data, the data file needs to be in the same folder as the script 
        #The input data will be an io text wrapper object, we will want to convert it a more convient form
        #While the io object is iterable it doesn't have a length so it splitting it into chunks based on the number
        #Of cores isn't feasible, but a list does have a length
        #Other data structures such as numpy arrays would also work, and if I had to chunk it myself would be much better
        #But a list is simple and works as the pool handles the chunking
        #Create a list to store the lines
        lineList = []
        #Then iterate through the lines in the input by opening the data file
        #I'm not threading this because I'm not sure how I would split up the io object to give to different threads
        #Or what chunk size I would use, as mentioned it doesn't have an accessable length
        #This could also have been done by assinging the io object to a variable and using .read() to create a giant string and then split it on the new line character
        #To separate each line, but I thought creating a gigantic string was a worse idea than this
        for line in open("AComp_Passenger_data_no_error(1).csv"):
            #And add the line to the list
            lineList.append(line)
        #With the input in a nicer format we can start to MapReduce
        #The map function of the pool object takes a function, data, and a size to split that data into
        #Then in parallel (using the specified number of cores) it applies the function to the split data
        #Helpfully it combines all of the output into a single object 
        #In this case it's applying the mapper function to the lines of input data
        #Which will clean them, extract and verify the passenger ID, then return the occurnace of that object
        #The chunk size is the length of the input list divided by the number of cores converted to an integer
        #As the result is likely a float which won't be a valid chunk size
        #The length of the input is 500 so many common core counts (4,8, etc) do not cleanly divide it
        #This can cause the code to hang but also be unstoppable meaning you have to close the program
        mapperOutput = pool.map(mapper, lineList, chunksize = int (len(lineList) / CORES))
        #mapperOutput is a list of tuples consiting of the passenger id and 1
        #To make reducing easier we can group together the passenger ids and counts
        #A dictionary is a very convient data structure for this, but when I tried to do this in parallel
        #It returned a list that looked like a dictionary rather than an actual dictionary
        #So the shuffling isn't done in parallel
        combined = combiner(mapperOutput)
        #shuffled is a dictionary with passenger ids as keys and lists of 1s as values
        #We reduce this dictionary to find out how many times each passenger has flown
        reduced = pool.map(reducer, combined.items(), chunksize = int (len(combined.keys()) / CORES))
        #reduced is a list of lists of keys/counts and it isn't sorted
        #To sort it we use a lambda function which uses the 2nd member of the list (the value) to sort the overall list
        #Sorting is in descending (reverse) order so the highest value is first
        reduced.sort(key = lambda x: x[1], reverse = True)
        #We then print this sorted list of lists to see which passenger has flown the most
        #I'm outputting the entire list, but specific values can be found by indexing the list
        #For the person with the most flights print(reduced[0]) and so on
        print(reduced)
        #Hopefully this is the same as what I get when I run it which is:
        #[['UES9151GS5', 25], ['PUD8209OG3', 23], ['BWI0520BG6', 23], ['DAZ3029XA0', 23], 
        #['SPR4484HA6', 23], ['EZC9678QI6', 21], ['HCA3158QA6', 21], ['JJM4724RF7', 21], 
        #['POP2875LH3', 19], ['WYU2010YH8', 19], ['CKZ3132BR4', 19], ['WBE6935NU3', 19], 
        #['HGO4350KK1', 18], ['CXN7304ER2', 17], ['YMH6360YP0', 16], ['JBE2302VO4', 16], 
        #['SJD8775RZ4', 16], ['VZY2993ME1', 16], ['LLZ3798PE3', 16], ['WTC9125IE5', 14], 
        #['MXU9187YC7', 14], ['EDV2089LK5', 13], ['XFG5747ZT9', 13], ['ONL0812DH1', 12], 
        #['CDC0302NN5', 12], ['CYJ0225CH1', 11], ['KKP5277HZ7', 11], ['PAJ3974RK1', 10], 
        #['IEG9308EA5', 10], ['PIT2755XC1', 8], ['UMH6360YP0', 1]]
