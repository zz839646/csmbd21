#!/usr/bin/env python

#Tutorial reducer, added functions, adjusted to work with combined output from mapper

import sys

def reducer(x, y):
    #Returns the sum of x and y
    return(x + y)

#The input for this will be in str form, like 'PIT2755XC1\t[1, 1, 1, 1, 1, 1, 1, 1]' 
#The commas and square brackets will need to be removed, this list will be used to control that process
formatting = ["[", "]", ","]
#Initalising some variables for the passenger ID and count
currentPID = ""
pid = ""
currentCount = 0
#Iterate through the input
for keys in sys.stdin:
    #Strip the input
    keys = keys.strip()
    #Iterate through the list of unwanted characters
    for x in formatting:
        #And replace each unwanted character with nothing
        keys = keys.replace(x, "")
    #Split the input on the tab
    pid, values = keys.split("\t")
    #The values portion of the input has spaces between each number so they are removed
    values = values.replace(" ", "")
    #Then check if the line is for the same passenger as the previous line
    if currentPID == pid:
        #If it is then loop through the values and reduce them with the current count
        for x in values:
            currentCount = reducer(int(currentCount), int(x))
    else:
        #If the line is for a new passenger first test if current count is not == 0
        #If it's zero then it's the first passenger and it would print 0 at the top of the output if this check was missing
        #This has the same effect as if currentCount but it's easier to understand like this
        if currentCount != 0:
            #If the is data to output then print the previous passenger id and count
            print('%s\t%s' % (currentPID, currentCount))
        #Reset the count
        currentCount = 0
        #Loop through the values
        for x in values:
            #And reduce them with the current count value
            currentCount = reducer(int(currentCount), int(x))
            #And replace the variable for passenger tracking
            currentPID = pid
        #Sort the output of the reducer based on the value of each key
        #First this uses sorted on the items of the dictionary, items returns a list of key = value tuples
        #These tuples are sorted in descending order using a lambda function that looks at the 2nd item of the key - value pair, the value
        #The output of sorted is also a list of key-value tuples so this is converted into a dictionary and then printed
        #So overall this line prints the reduced data sorted so the largest values are first
#print(dict(sorted(reduced.items(), key = lambda x: x[1], reverse = True)))

#Ensure the final passenger's information is printed because there won't be a next passenger so the previous passenger output code won't work
if currentPID == pid:
    print('%s\t%s' % (currentPID, currentCount))
