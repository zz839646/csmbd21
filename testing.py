#For testing in an IDE without running on hadoop

import sys
# Read input from STDIN (standard input)
mapoutput = []
lines = open("AComp_Passenger_data_no_error(1).csv")
#lines = ["UES9151GS5,SQU6245R,DEN,FRA,1420564460,1049", "UES9151GS5,SQU6245R,DEN,FRA,1420564460,1049", "UES9151GS5,XXQ4064B,JFK,FRA,1420563917,802"]
for line in lines:
    line = str(line)
    #Remove leading and trailing whitespace
    lineStripped = line.strip()
    # Write the results to STDOUT (standard output);
    # Tab-delimited; Key-Value pair <word, 1>
    cols = lineStripped.split(",")
    #The 6th column is the passenger id column, so print it's value, a tab, and 1
    mapoutput.append("%s\t%s" % (cols[5], 1))# Output here will be the input for the reduce
mapoutput.sort(key = lambda x: x[1])
PID = ""
currentPID = ""
currentCount = 0
#Iterate through the output of the mapper
for line in mapoutput:
    #Strip the input
    line = line.strip()
    #Then split the input on a tab, as that's how we separate the PID and count in the mapper
    #The first part of the input is the passenger ID, the second is the count
    PID, count = line.split("\t", 1)
    #Make sure the count is actually a number by converting it from string to an interger, if it fails it's not a number
    try:
        count = int(count)
    except ValueError:
        continue
    #Hadoop sorts the output so will be consecutive, so we check if the incoming word is the same as the previous
    if currentPID == PID:
        #If it is we keep summing the count
        currentCount += count
    else:
        #If we encounter a new word then we're done with the previous word, so we print that word and count
        print("%s\t%s" % (currentPID, currentCount))
        #Then start to look at the new word
        currentCount = count
        currentPID = PID
#For the final word the new word -> print old word logic won't run, so here out of the loop we print the final word and count here
if currentPID == PID:
    print("%s\t%s" % (currentPID, currentCount))
