#!/usr/bin/env python

#Tutorial mapper, adapted to the input file, added functions, added shuffler, adjusted output of shuffler

import sys

def mapper(x):
    #Returns a count of x
    return(x, 1)

def combiner(mapData):
    #Organises the output of a mapper by key
    #First create a dictionary
    combinedData = {}
    #Then go through the output of the mapper
    for x in mapData:
        key = x[0]
        value = x[1]
        #If the key is not in the dictionary
        if key not in combinedData.keys():
            #Add the value to a list with the key as the key
            combinedData[key] = [value]
        else:
            #Or append the new count to the list
            combinedData[key].append(value)
    #Then go through the keys in the dictionary
    for keys in combinedData.keys():
        #And print the key-value list pairs, this will go to the reducers
        print("%s\t%s" % (keys, combinedData[keys]))
    #This is implicit, but I'm making it explicit for readability
    return(None)

#A buffer file for the output of the mapper
mapOutput = []
#Iterate through lines in the input
for line in sys.stdin:
    #Strip the input
    line = line.strip()
    #And split it on commas as it's a csv file
    cols = line.split(",")
    #Add the mapped data to the buffer, the passenger id is in the first column so it's acessed with index 0
    mapOutput.append(mapper(cols[0]))
#Then combine the output of the mapper
combiner(mapOutput)
